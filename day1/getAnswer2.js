const data = require("./data.json");
const { input } = data;

let count = 0;
let actualNumber1;
let actualNumber2;
let actualNumber3;

input.forEach((number1) => {
  input.forEach((number2) => {
    input.forEach((number3) => {
      count++;
      const sum = parseInt(number1) + parseInt(number2) + parseInt(number3);

      if (parseInt(sum) == 2020) {
        actualNumber1 = number1;
        actualNumber2 = number2;
        actualNumber3 = number3;
      }
    });
  });
});

console.log("Answer: ", actualNumber1 * actualNumber2 * actualNumber3);