# AOC 2020

|Day|Completed|
|---|---|
|1|X|
|2|X|

## How to run?

1. Make sure you've got [node](https://nodejs.org/en/) installed.
2. Run `node getAnswer1.js`