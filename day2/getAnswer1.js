const fs = require('fs');

fs.readFile('./data.txt', 'utf8', function(err, data) {
  const lines = data.split(/\r?\n/);
  const passwordsAndPolicies = lines.map(line => {
    const values = line.split(' ');
    const range = values[0].split('-');

    return {
      rangeStart: range[0],
      rangeEnd: range[1],
      letter: values[1].slice(0, 1),
      password: values[2]
    }
  })

  let validPasswords = 0;

  passwordsAndPolicies.forEach(passpol => {
    let amount = 0;
    Array.from(passpol.password).forEach(letter => {
      if ( letter === passpol.letter ) {
        amount++;
      }
    })

    if (amount >= passpol.rangeStart && amount <= passpol.rangeEnd ) {
      validPasswords++;
    }
    amount = 0;
  })
  console.log('validPasswords:', validPasswords);
});