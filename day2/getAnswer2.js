const fs = require("fs");

fs.readFile("./data.txt", "utf8", function (err, data) {
  const lines = data.split(/\r?\n/);
  const passwordsAndPolicies = lines.map((line) => {
    const values = line.split(" ");
    const range = values[0].split("-");

    return {
      rangeStart: range[0],
      rangeEnd: range[1],
      letter: values[1].slice(0, 1),
      password: values[2],
    };
  });

  let validPasswords = 0;

  passwordsAndPolicies.forEach((passpol) => {
    const pos1 = Array.from(passpol.password)[passpol.rangeStart - 1];
    const pos2 = Array.from(passpol.password)[passpol.rangeEnd - 1];

    if ((passpol.letter === pos1 && passpol.letter !== pos2) || 
    (passpol.letter !== pos1 && passpol.letter === pos2)) {
      validPasswords++;
    }
  });
  console.log("validPasswords:", validPasswords);
});
